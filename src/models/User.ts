import axios, { AxiosResponse } from 'axios';

interface UserProps {
  id?: number;
  name?: string;
  age?: number;
}

const api = 'http://localhost:3000/users/'



export class User {

  constructor(private data: UserProps) { }

  get(propName: string): (number | string) {
    return this.data[propName];
  }

  set(update: UserProps): void {
    Object.assign(this.data, update)
  }


  fatch(): void {
    axios.get(`${api}${this.get('id')}`)
      .then((response: AxiosResponse): void => {
        this.set(response.data);
      })
  }

  save(): void {
    const id = this.get('id')

    if (id) {
      axios.put(`${api}${id}`, this.data)
    } else {
      axios.post(`${api}`, this.data)
    }

  }
}

